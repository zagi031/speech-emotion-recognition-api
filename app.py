from flask import Flask, jsonify, request
import librosa
import numpy as np
import joblib

app = Flask(__name__)


@app.route("/")
def welcome():
    return "<b>Welcome to speech emotion recognition API</b><br>" \
           "To predict emotion form .wav file, use /api/predict route."


@app.route("/api/predict", methods=['POST'])
def predict():
    if request.files:
        file = request.files['audioFile']
        y, sr = librosa.load(file, sr=44100)
        meanMFCCs = [np.mean(librosa.feature.mfcc(y=y, sr=44100, n_mfcc=20), axis=1)]
        classifier = joblib.load("svmClassifier.pkl")
        prediction = classifier.predict_proba(meanMFCCs).tolist()
        print(prediction)
        return jsonify(classes=classifier.classes_.tolist(), predictions=prediction[0])


if __name__ == "__main__":
    app.run(debug=True)
