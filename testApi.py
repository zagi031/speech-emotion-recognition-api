import requests

url_Local = "http://127.0.0.1:5000/api/predict"
url_External = "https://mzagorscak.pythonanywhere.com/api/predict"
filePath = "11a05Fc.wav"

with open(filePath, 'rb') as file:
    req = requests.post(url_External, files={"audioFile": file})
    print(req.status_code)
    print(req.text)
